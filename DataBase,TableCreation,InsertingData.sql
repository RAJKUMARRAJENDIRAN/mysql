use cbtech;
CREATE TABLE student (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    gender ENUM('Male', 'Female'),
    date_of_birth DATE,
    address VARCHAR(100),
    city VARCHAR(50),
    state VARCHAR(50),
    country VARCHAR(50),
    email VARCHAR(100),
    phone_number VARCHAR(20),
    enrollment_date DATE,
    graduation_date DATE,
    gpa DECIMAL(3, 2)
);

INSERT INTO student (first_name, last_name, gender, date_of_birth, address, city, state, country, email, phone_number, enrollment_date, graduation_date, gpa) VALUES
('John', 'Doe', 'Male', '2000-05-15', '123 Main St', 'Anytown', 'AnyState', 'USA', 'john.doe@example.com', '123-456-7890', '2020-09-01', '2024-05-30', 3.50),
('Jane', 'Smith', 'Female', '2001-03-20', '456 Elm St', 'Othertown', 'OtherState', 'USA', 'jane.smith@example.com', '987-654-3210', '2019-08-15', '2023-06-01', 3.75),
('Michael', 'Johnson', 'Male', '2000-12-10', '789 Oak St', 'Anycity', 'AnyState', 'USA', 'michael.johnson@example.com', '456-789-0123', '2021-01-05', '2025-06-15', 3.25),
('Emily', 'Williams', 'Female', '2002-02-25', '101 Pine St', 'Somewhere', 'SomeState', 'USA', 'emily.williams@example.com', '789-012-3456', '2022-03-10', '2026-05-20', 3.85),
('David', 'Brown', 'Male', '2001-07-05', '234 Maple St', 'Nowhere', 'NoState', 'USA', 'david.brown@example.com', '234-567-8901', '2023-02-20', '2027-06-30', 3.60),
('Sarah', 'Miller', 'Female', '2000-10-30', '567 Birch St', 'Anyplace', 'AnyState', 'USA', 'sarah.miller@example.com', '890-123-4567', '2020-06-15', '2024-05-15', 3.95),
('Matthew', 'Taylor', 'Male', '2001-05-08', '890 Cedar St', 'Nowhere', 'NoState', 'USA', 'matthew.taylor@example.com', '345-678-9012', '2021-09-01', '2025-06-01', 3.45),
('Olivia', 'Anderson', 'Female', '2002-08-12', '321 Walnut St', 'Anycity', 'AnyState', 'USA', 'olivia.anderson@example.com', '901-234-5678', '2022-08-15', '2026-05-30', 3.70),
('Daniel', 'Martinez', 'Male', '2000-04-18', '654 Ash St', 'Somewhere', 'SomeState', 'USA', 'daniel.martinez@example.com', '012-345-6789', '2023-01-05', '2027-06-15', 3.80),
('Sophia', 'Garcia', 'Female', '2001-09-22', '987 Pineapple St', 'Anytown', 'AnyState', 'USA', 'sophia.garcia@example.com', '567-890-1234', '2024-03-10', '2028-05-20', 3.55),
('Ethan', 'Hernandez', 'Male', '2000-11-28', '210 Orange St', 'Othertown', 'OtherState', 'USA', 'ethan.hernandez@example.com', '234-567-8901', '2020-01-20', '2024-06-30', 3.65),
('Isabella', 'Lopez', 'Female', '2002-06-03', '543 Banana St', 'Somewhere', 'SomeState', 'USA', 'isabella.lopez@example.com', '678-901-2345', '2021-05-15', '2025-05-15', 3.90),
('Alexander', 'King', 'Male', '2001-01-17', '876 Grape St', 'Nowhere', 'NoState', 'USA', 'alexander.king@example.com', '123-456-7890', '2022-08-01', '2026-06-01', 3.30),
('Mia', 'Perez', 'Female', '2000-07-20', '987 Cherry St', 'Anycity', 'AnyState', 'USA', 'mia.perez@example.com', '456-789-0123', '2023-04-10', '2027-06-15', 3.75),
('William', 'Gonzalez', 'Male', '2001-02-14', '654 Peach St', 'Anyplace', 'AnyState', 'USA', 'william.gonzalez@example.com', '789-012-3456', '2020-09-15', '2024-05-20', 3.40),
('Charlotte', 'Rodriguez', 'Female', '2002-05-27', '123 Plum St', 'Othertown', 'OtherState', 'USA', 'charlotte.rodriguez@example.com', '234-567-8901', '2021-03-01', '2025-05-30', 3.85),
('James', 'Lewis', 'Male', '2000-08-09', '876 Pear St', 'Anycity', 'AnyState', 'USA', 'james.lewis@example.com', '567-890-1234', '2022-10-20', '2026-06-30', 3.50),
('Amelia', 'Harris', 'Female', '2001-03-12', '987 Watermelon St', 'Somewhere', 'SomeState', 'USA', 'amelia.harris@example.com', '901-234-5678', '2023-02-15', '2027-05-15', 3.95)